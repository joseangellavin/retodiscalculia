/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retoDiscalculia.pizarras;

/**
 *
 * @author daw102
 */
public class GenNumeros {

    private int[] pareja1;
    private int[] pareja2;
    private int[] pareja3;
    private int[] pareja4;

    public GenNumeros() {
        pareja1 = new int[2];
        pareja2 = new int[2];
        pareja3 = new int[2];
        pareja4 = new int[2];

        boolean flag = false, flag1 = false, flag2 = false, flag3 = false;
        while (!flag) {
            pareja1[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
            while (!flag1) {
                
                pareja2[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
                
                if (pareja1[0] != pareja2[0]) {
                    
                    pareja2[1]=10-pareja2[0];
                    while (!flag2) {
                        
                        pareja3[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
                        
                        if ((pareja3[0] != pareja2[0]) && (pareja3[0] != pareja1[0])) {
                            
                            pareja3[1]=10-pareja3[0];
                            while (!flag3) {
                                
                                pareja4[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
                                if((pareja4[0]!=pareja3[0])&&(pareja4[0]!=pareja2[0])&&(pareja4[0]!=pareja1[0])){
                                    pareja4[1]=10-pareja4[0];
                                    flag3=true;
                                }
                            }
                            flag2=true;
                        }

                    }
                    
                    flag1 = true;
                }

            }
            pareja1[1]=10-pareja1[0];
            flag = true;

        }

    }

    public int[] getPareja1() {
        return pareja1;
    }

    public int[] getPareja2() {
        return pareja2;
    }

    public int[] getPareja3() {
        return pareja3;
    }

    public int[] getPareja4() {
        return pareja4;
    }
    
    
    public void mostrar(){
        String cadena="";
        cadena+=pareja1[0]+" | "+pareja1[1]+"\n";
        cadena+=pareja2[0]+" | "+pareja2[1]+"\n";
        cadena+=pareja3[0]+" | "+pareja3[1]+"\n";
        cadena+=pareja4[0]+" | "+pareja4[1];
        System.out.println(cadena);
    }
    
}
