/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retoDiscalculia.pizarras;

import java.awt.Color;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JLabel;
import javax.swing.JPanel;
import retoDiscalculia.Main;

/**
 *
 * @author Ivan Muinas
 */
public class Pizarras extends javax.swing.JDialog {

    /**
     * Creates new form Pizarras
     */
    GenNumeros numeros;
    ArrayList<String> nums;
    ArrayList<String> posiciones;
    ArrayList<String> numsElegir;
    ArrayList<String> posicionesElegir;
    int aciertos;
    int numeroFa;
    int numeroAc;
    JLabel selected = null;
    int time;
    int aux;
    boolean activate;
    Frame padre;
    int cont;

    /**
     * Inicializa todos los arrayList y establece los numeros generados aleatoriamente
     * por la clase GenNumeros en los labels y establece la duracion del juego
     * @param parent
     * @param modal
     * @param numDificultad establece la cantidad de tiempo que tendra el usuario para 
     * resolver los 4 problemas
     */
    
    public Pizarras(java.awt.Frame parent, boolean modal, int numDificultad) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(parent);
        numeros = new GenNumeros();
        nums = new ArrayList<String>();
        posiciones = new ArrayList<String>();
        numsElegir = new ArrayList<String>();
        posicionesElegir = new ArrayList<String>();
        actualizarBackgrounds();
        aciertos = 0;
        cargarNumsPizarra();
        asignarNumsPizarra();
        cargarNumsElegir();
        asignarNumsElegir();
        activate = false;

        padre = parent;
        cont = 0;

        numeroAc = 0;
        numeroFa = 0;
        numAciertos.setText(numeroAc + "");
        numFallos.setText(numeroFa + "");

        aux = numDificultad;
        time = aux;
        tiempo.setText(time + "");

        Timer tr = new Timer();

        TimerTask ts = new TimerTask() {
            @Override
            public void run() {

                time--;
                tiempo.setText(time + "");
                if (time == 0) {
                    reiniciar();
                }

            }
        };
        tr.schedule(ts, 0, 1000);
    }

    /**
     * reinicia los colores del fondo de los paneles
     */
    
    public void actualizarBackgrounds() {
        jPanel1.setBackground(new Color(0, 0, 0, 0));
        jPanel2.setBackground(new Color(0, 0, 0, 0));
        jPanel3.setBackground(new Color(0, 0, 0, 0));
        jPanel4.setBackground(new Color(0, 0, 0, 0));

    }
    
    /**
     * Carga los numeros generados por la clase GenNumeros al arrayList 'numeros' y las
     * posiciones en el arrayList 'posiciones'. Estos numeros seran los que iran en las
     * pizarras y no se podran mover
     */

    public void cargarNumsPizarra() {
        nums.add(numeros.getPareja1()[0] + "");
        nums.add(numeros.getPareja2()[0] + "");
        nums.add(numeros.getPareja3()[0] + "");
        nums.add(numeros.getPareja4()[0] + "");
        posiciones.add("0");
        posiciones.add("1");
        posiciones.add("2");
        posiciones.add("3");

    }

    /**
     * Saca un numero y una posicion aleatoria de los ArrayList 'numeros' y 'posiciones'
     * @return un array de 2 enteros con un numero y una posicion aleatorias
     */
    
    
    public int[] sacarNumeros() {
        int[] nume = new int[2];

        if (nums.size() != 0) {
            int aux1 = (int) Math.floor(Math.random() * nums.size());
            int aux2 = (int) Math.floor(Math.random() * posiciones.size());

            nume[0] = Integer.parseInt(nums.get(aux1));
            nume[1] = Integer.parseInt(posiciones.get(aux2));
            nums.remove(aux1);
            posiciones.remove(aux2);

        }
        return nume;
    }

    /**
     * establece los numeros aleatorios a los labels correspondientes
     */
    
    public void asignarNumsPizarra() {
        JLabel[] labels = {NumFijo1, NumFijo2, NumFijo3, NumFijo4};
        for (int i = 0; i < 4; i++) {
            int[] num = new int[2];

            num = sacarNumeros();
            labels[num[1]].setText(num[0] + "");

        }
    }
    
    /**
     * Carga los numeros generados por la clase GenNumeros al arrayList 'numeros' y las
     * posiciones en el arrayList 'posiciones'. Estos numeros seran los que el usuario
     * debera elegir
     */
    public void cargarNumsElegir() {
        numsElegir.add(numeros.getPareja1()[1] + "");
        numsElegir.add(numeros.getPareja2()[1] + "");
        numsElegir.add(numeros.getPareja3()[1] + "");
        numsElegir.add(numeros.getPareja4()[1] + "");
        posicionesElegir.add("0");
        posicionesElegir.add("1");
        posicionesElegir.add("2");
        posicionesElegir.add("3");
    }

    /**
     * Saca un numero y una posicion aleatoria de los ArrayList 'numeros' y 'posiciones'
     * @return un array de 2 enteros con un numero y una posicion aleatorias
     */
    
    public int[] sacarNumerosElegir() {
        int[] nume = new int[2];

        if (numsElegir.size() != 0) {
            int aux1 = (int) Math.floor(Math.random() * numsElegir.size());
            int aux2 = (int) Math.floor(Math.random() * posicionesElegir.size());

            nume[0] = Integer.parseInt(numsElegir.get(aux1));
            nume[1] = Integer.parseInt(posicionesElegir.get(aux2));
            numsElegir.remove(aux1);
            posicionesElegir.remove(aux2);

        }
        return nume;
    }

    
    /**
     * establece los numeros aleatorios a los labels correspondientes
     */
    public void asignarNumsElegir() {
        JLabel[] labels = {num1, num2, num3, num4};
        for (int i = 0; i < 4; i++) {
            int[] num = new int[2];

            num = sacarNumerosElegir();
            //System.out.println(num[1] + " | " + num[0]);
            labels[num[1]].setText(num[0] + "");

        }
    }
    /**
     * reinicia el juego entero a como venia por defecto
     */
    public void reiniciar() {
        numeros = new GenNumeros();
        nums = new ArrayList<String>();
        posiciones = new ArrayList<String>();
        numsElegir = new ArrayList<String>();
        posicionesElegir = new ArrayList<String>();

        aciertos = 0;
        cargarNumsPizarra();
        asignarNumsPizarra();
        cargarNumsElegir();
        asignarNumsElegir();
        reiniciarPaneles();
        activate = false;
        time = aux;
        tiempo.setText(time + "");
    }

    /**
     * reinia los paneles a como venia por defecto
     */
    
    public void reiniciarPaneles() {
        NumResul1.setText("");
        NumResul2.setText("");
        NumResul3.setText("");
        NumResul4.setText("");
        num1.setForeground(Color.black);
        num2.setForeground(Color.black);
        num3.setForeground(Color.black);
        num4.setForeground(Color.black);
    }
    
    /**
     * Muestra de clor rojo o verde si la opcion es correcta o incorrecta
    * @param JPanel recibe el panel donde se mostraran los cambios
    * @param boolean Si el valor es true muestra una panel de color verde y si es falso
    */

    private void opcionCorrecta(JPanel p,boolean opcion) {
        if(opcion){
            p.setBackground(Color.green);
        }else{
            p.setBackground(Color.red);
        }
        
        
        Timer tr1 = new Timer();

        TimerTask ts1 = new TimerTask() {
            @Override
            public void run() {

                cont++;
                if (cont == 2) {
                    p.setBackground(Color.white);
                    cont=0;
                    tr1.cancel();
                    this.cancel();
                    
                }
            }
        };
        tr1.schedule(ts1, 0, 500);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelEmpiece = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        Ppiz2 = new javax.swing.JPanel();
        Ppiz1 = new javax.swing.JPanel();
        Ppiz4 = new javax.swing.JPanel();
        Ppiz3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        NumFijo1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        NumResul1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        NumFijo2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        NumResul2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        NumFijo3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        NumResul3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        NumFijo4 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        NumResul4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        num1 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        num2 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        num3 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        num4 = new javax.swing.JLabel();
        LTiempo = new javax.swing.JLabel();
        PTiempo = new javax.swing.JPanel();
        tiempo = new javax.swing.JLabel();
        LAciertos = new javax.swing.JLabel();
        PAciertos = new javax.swing.JPanel();
        numAciertos = new javax.swing.JLabel();
        LFallos = new javax.swing.JLabel();
        PFallos = new javax.swing.JPanel();
        numFallos = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(798, 748));
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        PanelEmpiece.setBackground(new java.awt.Color(0, 204, 204));
        PanelEmpiece.setMinimumSize(new java.awt.Dimension(798, 748));
        PanelEmpiece.setName(""); // NOI18N
        PanelEmpiece.setPreferredSize(new java.awt.Dimension(798, 748));
        PanelEmpiece.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton3.setText("Salir");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        PanelEmpiece.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 10, -1, -1));

        jButton4.setText("Volver al menu");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        PanelEmpiece.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        Ppiz2.setBackground(new java.awt.Color(255, 255, 255));
        Ppiz2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        javax.swing.GroupLayout Ppiz2Layout = new javax.swing.GroupLayout(Ppiz2);
        Ppiz2.setLayout(Ppiz2Layout);
        Ppiz2Layout.setHorizontalGroup(
            Ppiz2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 46, Short.MAX_VALUE)
        );
        Ppiz2Layout.setVerticalGroup(
            Ppiz2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 36, Short.MAX_VALUE)
        );

        PanelEmpiece.add(Ppiz2, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 180, 50, 40));

        Ppiz1.setBackground(new java.awt.Color(255, 255, 255));
        Ppiz1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        javax.swing.GroupLayout Ppiz1Layout = new javax.swing.GroupLayout(Ppiz1);
        Ppiz1.setLayout(Ppiz1Layout);
        Ppiz1Layout.setHorizontalGroup(
            Ppiz1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 46, Short.MAX_VALUE)
        );
        Ppiz1Layout.setVerticalGroup(
            Ppiz1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 36, Short.MAX_VALUE)
        );

        PanelEmpiece.add(Ppiz1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 180, 50, 40));

        Ppiz4.setBackground(new java.awt.Color(255, 255, 255));
        Ppiz4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        javax.swing.GroupLayout Ppiz4Layout = new javax.swing.GroupLayout(Ppiz4);
        Ppiz4.setLayout(Ppiz4Layout);
        Ppiz4Layout.setHorizontalGroup(
            Ppiz4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 46, Short.MAX_VALUE)
        );
        Ppiz4Layout.setVerticalGroup(
            Ppiz4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 36, Short.MAX_VALUE)
        );

        PanelEmpiece.add(Ppiz4, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 350, 50, 40));

        Ppiz3.setBackground(new java.awt.Color(255, 255, 255));
        Ppiz3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        javax.swing.GroupLayout Ppiz3Layout = new javax.swing.GroupLayout(Ppiz3);
        Ppiz3.setLayout(Ppiz3Layout);
        Ppiz3Layout.setHorizontalGroup(
            Ppiz3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 46, Short.MAX_VALUE)
        );
        Ppiz3Layout.setVerticalGroup(
            Ppiz3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 36, Short.MAX_VALUE)
        );

        PanelEmpiece.add(Ppiz3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 350, 50, 40));

        jPanel1.setBackground(new java.awt.Color(204, 153, 0));
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        NumFijo1.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumFijo1.setForeground(new java.awt.Color(255, 255, 255));
        NumFijo1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(NumFijo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 58, 56));

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("+");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 58, 56));

        NumResul1.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumResul1.setForeground(new java.awt.Color(255, 255, 255));
        NumResul1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(NumResul1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 58, 56));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/retoDiscalculia/pizarras/pizarra.PNG"))); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 4, 250, 130));

        PanelEmpiece.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(98, 127, -1, -1));

        jPanel2.setBackground(new java.awt.Color(204, 153, 0));
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel2MouseClicked(evt);
            }
        });
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        NumFijo2.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumFijo2.setForeground(new java.awt.Color(255, 255, 255));
        NumFijo2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel2.add(NumFijo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 58, 56));

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("+");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 58, 56));

        NumResul2.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumResul2.setForeground(new java.awt.Color(255, 255, 255));
        NumResul2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel2.add(NumResul2, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 58, 56));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/retoDiscalculia/pizarras/pizarra.PNG"))); // NOI18N
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 4, 250, 130));

        PanelEmpiece.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(446, 127, -1, -1));

        jPanel3.setBackground(new java.awt.Color(204, 153, 0));
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel3MouseClicked(evt);
            }
        });
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        NumFijo3.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumFijo3.setForeground(new java.awt.Color(255, 255, 255));
        NumFijo3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel3.add(NumFijo3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 58, 56));

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("+");
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 58, 56));

        NumResul3.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumResul3.setForeground(new java.awt.Color(255, 255, 255));
        NumResul3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel3.add(NumResul3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 58, 56));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/retoDiscalculia/pizarras/pizarra.PNG"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 4, 250, 130));

        PanelEmpiece.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(98, 296, -1, -1));

        jPanel4.setBackground(new java.awt.Color(204, 153, 0));
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel4MouseClicked(evt);
            }
        });
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        NumFijo4.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumFijo4.setForeground(new java.awt.Color(255, 255, 255));
        NumFijo4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel4.add(NumFijo4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 58, 56));

        jLabel14.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("+");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 58, 56));

        NumResul4.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        NumResul4.setForeground(new java.awt.Color(255, 255, 255));
        NumResul4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel4.add(NumResul4, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 58, 56));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/retoDiscalculia/pizarras/pizarra.PNG"))); // NOI18N
        jPanel4.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 4, 250, 130));

        PanelEmpiece.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(446, 296, -1, -1));

        jPanel5.setBackground(new java.awt.Color(51, 51, 51));

        jPanel6.setBackground(new java.awt.Color(0, 255, 102));

        num1.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        num1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        num1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                num1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        jPanel7.setBackground(new java.awt.Color(0, 255, 102));

        num2.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        num2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        num2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                num2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num2, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num2, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        jPanel8.setBackground(new java.awt.Color(0, 255, 102));

        num3.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        num3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        num3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                num3MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num3, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num3, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(0, 255, 102));

        num4.setFont(new java.awt.Font("Dialog", 1, 64)); // NOI18N
        num4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        num4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                num4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num4, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(num4, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelEmpiece.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(98, 457, 602, -1));

        LTiempo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LTiempo.setText("Tiempo: ");
        PanelEmpiece.add(LTiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 640, -1, 25));

        PTiempo.setBackground(new java.awt.Color(51, 51, 51));
        PTiempo.setPreferredSize(new java.awt.Dimension(50, 50));

        tiempo.setBackground(new java.awt.Color(51, 51, 51));
        tiempo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tiempo.setForeground(new java.awt.Color(255, 255, 255));
        tiempo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PTiempoLayout = new javax.swing.GroupLayout(PTiempo);
        PTiempo.setLayout(PTiempoLayout);
        PTiempoLayout.setHorizontalGroup(
            PTiempoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tiempo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );
        PTiempoLayout.setVerticalGroup(
            PTiempoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tiempo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        PanelEmpiece.add(PTiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 630, -1, -1));

        LAciertos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LAciertos.setText("Aciertos:");
        PanelEmpiece.add(LAciertos, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 610, -1, -1));

        PAciertos.setBackground(new java.awt.Color(51, 51, 51));
        PAciertos.setPreferredSize(new java.awt.Dimension(50, 50));

        numAciertos.setBackground(new java.awt.Color(51, 51, 51));
        numAciertos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        numAciertos.setForeground(new java.awt.Color(255, 255, 255));
        numAciertos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PAciertosLayout = new javax.swing.GroupLayout(PAciertos);
        PAciertos.setLayout(PAciertosLayout);
        PAciertosLayout.setHorizontalGroup(
            PAciertosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(numAciertos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
        );
        PAciertosLayout.setVerticalGroup(
            PAciertosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(numAciertos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        PanelEmpiece.add(PAciertos, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 600, 88, -1));

        LFallos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LFallos.setText("Fallos:");
        PanelEmpiece.add(LFallos, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 670, -1, -1));

        PFallos.setBackground(new java.awt.Color(51, 51, 51));
        PFallos.setPreferredSize(new java.awt.Dimension(50, 50));

        numFallos.setBackground(new java.awt.Color(51, 51, 51));
        numFallos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        numFallos.setForeground(new java.awt.Color(255, 255, 255));
        numFallos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PFallosLayout = new javax.swing.GroupLayout(PFallos);
        PFallos.setLayout(PFallosLayout);
        PFallosLayout.setHorizontalGroup(
            PFallosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(numFallos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
        );
        PFallosLayout.setVerticalGroup(
            PFallosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(numFallos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        PanelEmpiece.add(PFallos, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 660, 88, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Suma 10");
        PanelEmpiece.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 32, 798, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Selecciona uno de los números de abajo y después selecciona la pizarra que corresponda");
        PanelEmpiece.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 82, 798, -1));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/retoDiscalculia/pizarras/fondo.png"))); // NOI18N
        PanelEmpiece.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(-6, -6, 810, 810));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelEmpiece, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelEmpiece, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        int num1 = Integer.parseInt(NumFijo1.getText());
        if (selected != null) {
            int num2 = Integer.parseInt(selected.getText());
            if ((num1 + num2) == 10) {
                NumResul1.setText(num2 + "");
                selected.setText("");
                selected = null;
                aciertos++;
                numeroAc++;
                numAciertos.setText(numeroAc + "");
                opcionCorrecta(Ppiz1,true);
                if (aciertos == 4) {
                    reiniciar();
                }
            } else {
                numeroFa++;
                numFallos.setText(numeroFa + "");
                opcionCorrecta(Ppiz1,false);
                selected.setForeground(Color.black);
                selected = null;
            }

        }
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jPanel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseClicked
        int num1 = Integer.parseInt(NumFijo2.getText());
        if (selected != null) {
            int num2 = Integer.parseInt(selected.getText());
            if ((num1 + num2) == 10) {
                NumResul2.setText(num2 + "");
                selected.setText("");
                selected = null;
                aciertos++;
                numeroAc++;
                numAciertos.setText(numeroAc + "");
                opcionCorrecta(Ppiz2,true);
                if (aciertos == 4) {
                    reiniciar();
                }
            } else {
                numeroFa++;
                numFallos.setText(numeroFa + "");
                opcionCorrecta(Ppiz2,false);
                selected.setForeground(Color.black);
                selected = null;
            }
        }
    }//GEN-LAST:event_jPanel2MouseClicked

    private void jPanel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseClicked
        int num1 = Integer.parseInt(NumFijo3.getText());
        if (selected != null) {
            int num2 = Integer.parseInt(selected.getText());
            if ((num1 + num2) == 10) {
                NumResul3.setText(num2 + "");
                selected.setText("");
                selected = null;
                aciertos++;
                numeroAc++;
                numAciertos.setText(numeroAc + "");

                opcionCorrecta(Ppiz3,true);
                if (aciertos == 4) {
                    reiniciar();
                }
            } else {
                numeroFa++;
                numFallos.setText(numeroFa + "");
                opcionCorrecta(Ppiz3,false);
                selected.setForeground(Color.black);
                selected = null;
            }
        }

    }//GEN-LAST:event_jPanel3MouseClicked

    private void jPanel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseClicked
        int num1 = Integer.parseInt(NumFijo4.getText());
        if (selected != null) {
            int num2 = Integer.parseInt(selected.getText());
            if ((num1 + num2) == 10) {
                NumResul4.setText(num2 + "");
                selected.setText("");
                selected = null;
                aciertos++;
                numeroAc++;
                numAciertos.setText(numeroAc + "");
                opcionCorrecta(Ppiz4,true);
                if (aciertos == 4) {
                    reiniciar();
                }
            } else {
                numeroFa++;
                numFallos.setText(numeroFa + "");
                opcionCorrecta(Ppiz4,false);
                selected.setForeground(Color.black);
                selected = null;
            }
        }
    }//GEN-LAST:event_jPanel4MouseClicked

    private void num1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_num1MouseClicked
        if (selected != null) {
            selected.setForeground(Color.black);
        }
        if (!activate) {
            selected = num1;
            selected.setForeground(Color.white);
            activate = true;
        } else {
            selected = null;
            activate = false;
        }
    }//GEN-LAST:event_num1MouseClicked

    private void num2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_num2MouseClicked
        if (selected != null) {
            selected.setForeground(Color.black);
        }
        if (!activate) {
            selected = num2;
            selected.setForeground(Color.white);
            activate = true;
        } else {
            selected = null;
            activate = false;
        }
    }//GEN-LAST:event_num2MouseClicked

    private void num3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_num3MouseClicked
        if (selected != null) {
            selected.setForeground(Color.black);
        }
        if (!activate) {
            selected = num3;
            selected.setForeground(Color.white);
            activate = true;
        } else {
            selected = null;
            activate = false;
        }
    }//GEN-LAST:event_num3MouseClicked

    private void num4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_num4MouseClicked
        if (selected != null) {
            selected.setForeground(Color.black);
        }
        if (!activate) {
            selected = num4;
            selected.setForeground(Color.white);
            activate = true;
        } else {
            selected = null;
            activate = false;
        }
    }//GEN-LAST:event_num4MouseClicked

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        this.dispose();
        Main m = new Main();
        m.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pizarras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pizarras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pizarras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pizarras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Pizarras dialog = new Pizarras(new javax.swing.JFrame(), true, 15);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LAciertos;
    private javax.swing.JLabel LFallos;
    private javax.swing.JLabel LTiempo;
    private javax.swing.JLabel NumFijo1;
    private javax.swing.JLabel NumFijo2;
    private javax.swing.JLabel NumFijo3;
    private javax.swing.JLabel NumFijo4;
    private javax.swing.JLabel NumResul1;
    private javax.swing.JLabel NumResul2;
    private javax.swing.JLabel NumResul3;
    private javax.swing.JLabel NumResul4;
    private javax.swing.JPanel PAciertos;
    private javax.swing.JPanel PFallos;
    private javax.swing.JPanel PTiempo;
    private javax.swing.JPanel PanelEmpiece;
    private javax.swing.JPanel Ppiz1;
    private javax.swing.JPanel Ppiz2;
    private javax.swing.JPanel Ppiz3;
    private javax.swing.JPanel Ppiz4;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JLabel num1;
    private javax.swing.JLabel num2;
    private javax.swing.JLabel num3;
    private javax.swing.JLabel num4;
    private javax.swing.JLabel numAciertos;
    private javax.swing.JLabel numFallos;
    private javax.swing.JLabel tiempo;
    // End of variables declaration//GEN-END:variables
}
