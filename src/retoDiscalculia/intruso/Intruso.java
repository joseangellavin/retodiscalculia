/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retoDiscalculia.intruso;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import retoDiscalculia.Main;

/**
 *
 * @author Sergio San Emeterio Sañudo
 * @since 20/05/2019
 * @version 1.1
 */
public class Intruso extends javax.swing.JDialog implements Runnable {

    /**
     * Atributos de la clase Intruso
     */
    int botonIncorrecto;
    int contador = 0;
    int fallos = 0;
    int temporizador = 10;
    Thread hilo;
    ArrayList<String> numeros;
    ArrayList<String> posiciones;
    Frame padre;
    Timer t;
    public Intruso(java.awt.Frame parent, boolean modal) {
        /**
         * Prohibimos que se pueda redimensionar Establecemos el titulo a la
         * aplicacion Adaptamos las imagenes, una para el fondo y otra para el
         * temporizador. Sacamos un dialogo informativo al iniciar la aplicación
         * Establecemos en los jLabel el contador de aciertos y fallos.
         * Establecemos tambien un timer para decrementar el relojero de 10 a 0
         * en tiempo de ejecución
         */
        super(parent, modal);
        initComponents();

        this.setResizable(false);
        this.setTitle("Minijuego número intruso");
        this.setLocationRelativeTo(null);

        padre = parent;

        numeros = new ArrayList<>();
        posiciones = new ArrayList<>();

        adapta(jLabelReloj, new ImageIcon(getClass().getResource("/retoDiscalculia/intruso/reloj.png")));
        adapta(jLabelFondo, new ImageIcon(getClass().getResource("/retoDiscalculia/intruso/fondo.jpg")));

//        JOptionPane.showMessageDialog(null, "Haz click sobre el número que sobra para que sumen 10.\n"
//                + "Date prisa, debes hacerlo antes de que se acabe el tiempo.", "INSTRUCCIONES",0,null);
        //JOptionPane.showMessageDialog(null, "Haz click sobre el número que sobra para que sumen 10.\n"
        //        + "Date prisa, debes hacerlo antes de que se acabe el tiempo.", "INSTRUCCIONES IMPORTANTES", JOptionPane.WARNING_MESSAGE);
        genera();

        Aciertos.setText("Aciertos: " + String.valueOf(contador));
        Fallos.setText("Fallos: " + String.valueOf(fallos));
        hilo = new Thread(this);
        hilo.start();
        t = new Timer(1000, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                cambia();
                
            }
        });
        t.start();
    }

    /**
     * Adaptamos las imagenes al label.
     *
     * @param label
     * @param img
     */
    private void adapta(JLabel label, ImageIcon img) {

        Icon icon = new ImageIcon(img.getImage().
                getScaledInstance(label.getWidth(),
                        label.getHeight(), img.getIconWidth()));
        label.setIcon(icon);

    }
    /**
     * reinicia el juego y el temporizador
     */
    public void reiniciar() {
        genera();
        temporizador = 10;
    }

    /**
     * Le damos al label el valor del temporizador Cuando llegue a 0, cerramos
     * la aplicación
     */
    public void cambia() {

        jLabelTemporizador.setText(String.valueOf(temporizador));

        temporizador--;

        if (temporizador == 0) {

            JOptionPane.showMessageDialog(null, "¡¡HAS PERDIDO!!", "Fin del juego", 0, null);
            this.dispose();
            MensajeReinicio m = new MensajeReinicio(padre, rootPaneCheckingEnabled);
            m.setVisible(true);
        }

    }

    /**
     * Generacion de numeros aleatorios en variables numericas de tipo entero.
     * Comprobacion de que el tercer numero no es igual a los otros dos.
     */
    public void genera() {

        int primero = 0;
        int segundo = 0;
        int tercero = 0;

        temporizador = 10;

        boolean flag = false;

        while (!flag) {
            primero = ((int) Math.floor(Math.random() * 9) + 1);
            segundo = 10 - primero;
            boolean flag2 = false;
            while (!flag2) {
                tercero = ((int) Math.floor(Math.random() * 9) + 1);
                if ((tercero != primero) && (tercero != segundo)) {
                    flag2 = true;
                    flag = true;
                }
            }
        }
        cargarArrays(primero, segundo, tercero);
        asignarNumeros();

        int aux1 = Integer.parseInt(jButton1.getText());
        int aux2 = Integer.parseInt(jButton2.getText());
        int aux3 = Integer.parseInt(jButton3.getText());
        if ((aux1 + aux2) == 10) {
            botonIncorrecto = 3;
        } else if ((aux1 + aux3) == 10) {
            botonIncorrecto = 2;
        } else {
            botonIncorrecto = 1;
        }
        cambia();
    }
    
   /**
    * Inicializa los ArrayList 'numeros' y 'posiciones'
    * @param num1 recibe el primer numero aleatorio
    * @param num2 recibe el segundo numero aleatorio
    * @param num3 recibe el tercer numero aleatorio
    */
    
    public void cargarArrays(int num1, int num2, int num3) {
        posiciones.add("0");
        posiciones.add("1");
        posiciones.add("2");
        numeros.add(num1 + "");
        numeros.add(num2 + "");
        numeros.add(num3 + "");
    }

    /**
     * saca un numero y una posicion aleatoria
     * @return un array de 2 enteros con uno de los numeros aleatorios y su posicion
     */
    
    public int[] sacarNumeros() {
        int[] nume = new int[2];

        if (numeros.size() != 0) {
            int aux1 = (int) Math.floor(Math.random() * numeros.size());
            int aux2 = (int) Math.floor(Math.random() * posiciones.size());

            nume[0] = Integer.parseInt(numeros.get(aux1));
            nume[1] = Integer.parseInt(posiciones.get(aux2));
            numeros.remove(aux1);
            posiciones.remove(aux2);

        }
        return nume;
    }

    /**
     * Asigna a los botones los valores aleatorios
     */
    
    public void asignarNumeros() {
        JButton[] botones = {jButton1, jButton2, jButton3};
        for (int i = 0; i < 3; i++) {
            int[] num = new int[2];

            num = sacarNumeros();
            botones[num[1]].setText(num[0] + "");

        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        returnBIntruso = new javax.swing.JButton();
        jLabelTemporizador = new javax.swing.JLabel();
        Cabecera = new javax.swing.JLabel();
        jLabelReloj = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        Aciertos = new javax.swing.JLabel();
        Fallos = new javax.swing.JLabel();
        jLabelFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(580, 323));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(580, 323));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setPreferredSize(new java.awt.Dimension(580, 323));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton4.setText("Salir");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 280, -1, -1));

        returnBIntruso.setText("Volver al menu");
        returnBIntruso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnBIntrusoActionPerformed(evt);
            }
        });
        jPanel1.add(returnBIntruso, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, -1, -1));

        jLabelTemporizador.setFont(new java.awt.Font("Papyrus", 1, 36)); // NOI18N
        jLabelTemporizador.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTemporizador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(jLabelTemporizador, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 70, 60));

        Cabecera.setFont(new java.awt.Font("Papyrus", 1, 18)); // NOI18N
        Cabecera.setText("Encuentra al intruso");
        jPanel1.add(Cabecera, new org.netbeans.lib.awtextra.AbsoluteConstraints(185, 11, 190, -1));
        jPanel1.add(jLabelReloj, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 100, 80));

        jButton1.setBackground(new java.awt.Color(51, 102, 255));
        jButton1.setFont(new java.awt.Font("Papyrus", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 200, 100, 50));

        jButton2.setBackground(new java.awt.Color(51, 102, 255));
        jButton2.setFont(new java.awt.Font("Papyrus", 1, 18)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 200, 100, 50));

        jButton3.setBackground(new java.awt.Color(51, 102, 255));
        jButton3.setFont(new java.awt.Font("Papyrus", 1, 18)); // NOI18N
        jButton3.setForeground(new java.awt.Color(255, 255, 255));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 200, 100, 50));

        Aciertos.setFont(new java.awt.Font("Papyrus", 1, 18)); // NOI18N
        Aciertos.setForeground(new java.awt.Color(255, 0, 0));
        jPanel1.add(Aciertos, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 0, 150, 49));

        Fallos.setFont(new java.awt.Font("Papyrus", 1, 18)); // NOI18N
        Fallos.setForeground(new java.awt.Color(255, 51, 51));
        jPanel1.add(Fallos, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, 150, 40));

        jLabelFondo.setPreferredSize(new java.awt.Dimension(580, 323));
        jPanel1.add(jLabelFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 320));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 320));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Metodos que se encargan de sumar o restar puntos en funcion del acierto o
     * fallo
     *
     * @param evt
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if (botonIncorrecto == 1) {

            contador++;
            Aciertos.setText("Aciertos: " + String.valueOf(contador));
            genera();
        } else if ((botonIncorrecto == 2) || (botonIncorrecto == 3)) {
            fallos++;
            Fallos.setText("Fallos: " + String.valueOf(fallos));
            genera();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * Metodos que se encargan de sumar o restar puntos en funcion del acierto o
     * fallo
     *
     * @param evt
     */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (botonIncorrecto == 2) {

            contador++;
            Aciertos.setText("Aciertos: " + String.valueOf(contador));
            genera();
        } else if ((botonIncorrecto == 1) || (botonIncorrecto == 3)) {
            fallos++;
            Fallos.setText("Fallos: " + String.valueOf(fallos));
            genera();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * Metodos que se encargan de sumar o restar puntos en funcion del acierto o
     * fallo
     *
     * @param evt
     */
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if (botonIncorrecto == 3) {

            contador++;
            Aciertos.setText("Aciertos: " + String.valueOf(contador));
            genera();
        } else if ((botonIncorrecto == 2) || (botonIncorrecto == 1)) {
            fallos++;
            Fallos.setText("Fallos: " + String.valueOf(fallos));
            genera();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void returnBIntrusoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_returnBIntrusoActionPerformed
        t.stop();
        this.dispose();
        Main m = new Main();
        m.setVisible(true);
    }//GEN-LAST:event_returnBIntrusoActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        t.stop();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // Espera de 10 segundos para que el programa no termine
        // inmediatamente
//         try
//         {
//            Thread.currentThread().sleep (10000);
//         } catch (Exception e) {
//         }
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Intruso.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Intruso.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Intruso.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Intruso.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                Intruso dialog = new Intruso(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Aciertos;
    private javax.swing.JLabel Cabecera;
    private javax.swing.JLabel Fallos;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabelFondo;
    private javax.swing.JLabel jLabelReloj;
    private javax.swing.JLabel jLabelTemporizador;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton returnBIntruso;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {

    }
}
