/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retoDiscalculia.carreraCoches;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author José Ángel Lavín Fernández
 */
public class Juego extends javax.swing.JDialog implements Runnable {

    /**
     * Metodo que nos permite establecer como fondo de una label una imagen
     * pasadas como argumentos.
     *
     * @param label label que va a ser modificada
     * @param img Imagen que se establecera como fondo de la label
     */
    protected void adapta(JLabel label, ImageIcon img) {
        Icon icon = new ImageIcon(img.getImage().
                getScaledInstance(label.getWidth(),
                        label.getHeight(), img.getIconWidth()));
        label.setIcon(icon);

    }
    /**
     * Variable que nos permite crear la animacion de los coches.
     */
    Thread hilo;
    /**
     * Array de coches que nos permitira manejar de manera mas sencilla los
     * coches para su movimiento.
     */
    JLabel[] coches = new JLabel[3];
    /**
     * Numero entero que se utilizara para seleccionar que boton contiene la
     * solucion de la operacion.
     */
    int botonCorrecto = 0;

    /**
     * Constructor de la clase
     *
     * @param parent 
     * @param modal 
     */
    public Juego(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        hilo = new Thread(this);
        this.setLocationRelativeTo(this);
        this.setResizable(false);
        adaptaciones();
        inicio();

    }

    /**
     * Metodo que establece el titulo del dialogo, añade los coches openentes al
     * array creado previamente y oculta algunos elementos.
     */
    protected void inicio() {
        switch (MenuCarrera.operacion) {
            case 1:
                this.setTitle("Carrera de coches  -  Sumas");
                break;
            case 2:
                this.setTitle("Carrera de coches  -  Restas");
                break;
            case 3:
                this.setTitle("Carrera de coches  -  Multiplicaciones");
                break;
            case 4:
                this.setTitle("Carrera de coches  -  Divisiones");
                break;
        }
        coches[0] = jLabelC2;
        coches[1] = jLabelC3;
        coches[2] = jLabelC4;
        jPanelMensaje.setVisible(false);
        jPanelCoches.setVisible(false);
        jLabelNube.setVisible(false);
        jButtonSol1.setVisible(false);
        jButtonSol2.setVisible(false);
        jButtonSol3.setVisible(false);
        jLabelOperacion.setVisible(false);
    }

    /**
     * Metodo que establece el fondo a las labels
     */
    protected void adaptaciones() {
        ImageIcon[] rutas = {
            new ImageIcon(getClass().getResource("/retoDiscalculia/carreraCoches/img/fondo1.png")),
            new ImageIcon(getClass().getResource("/retoDiscalculia/carreraCoches/img/coche1.png")),
            new ImageIcon(getClass().getResource("/retoDiscalculia/carreraCoches/img/coche2.png")),
            new ImageIcon(getClass().getResource("/retoDiscalculia/carreraCoches/img/coche3.png")),
            new ImageIcon(getClass().getResource("/retoDiscalculia/carreraCoches/img/coche4.png")),
            new ImageIcon(getClass().getResource("/retoDiscalculia/carreraCoches/img/nube1.png")
            )};
        adapta(jLabelFondo, rutas[0]);
        adapta(jLabelC1, rutas[1]);
        adapta(jLabelC2, rutas[2]);
        adapta(jLabelC3, rutas[3]);
        adapta(jLabelC4, rutas[4]);
        adapta(jLabelNube, rutas[5]);

    }

    /**
     * Metodo que genera la operacion matematica en funcion del valor de la
     * variable operacion, obtenida de la clase MenuCarrera.
     */
    protected void operacion() {
        int n1, n2, boton;
        switch (MenuCarrera.operacion) {
            case 1:
                n1 = Funciones.obtenerNumero();
                n2 = Funciones.obtenerNumero();
                boton = Funciones.selectBoton();
                jLabelOperacion.setText(n1 + " + " + n2);
                switch (boton) {
                    case 1:
                        botonCorrecto = 1;
                        jButtonSol1.setText(Integer.toString(n1 + n2));
                        jButtonSol2.setText(Integer.toString(n1 + n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 + n2 - Funciones.aleatorio()));
                        break;
                    case 2:
                        botonCorrecto = 2;
                        jButtonSol2.setText(Integer.toString(n1 + n2));
                        jButtonSol1.setText(Integer.toString(n1 + n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 + n2 - Funciones.aleatorio()));
                        break;
                    case 3:
                        botonCorrecto = 3;
                        jButtonSol3.setText(Integer.toString(n1 + n2));
                        jButtonSol1.setText(Integer.toString(n1 + n2 + Funciones.aleatorio()));
                        jButtonSol2.setText(Integer.toString(n1 + n2 - Funciones.aleatorio()));
                        break;
                }
                break;

            case 2:
                do {
                    n1 = Funciones.obtenerNumero();
                    n2 = Funciones.obtenerNumero();
                } while ((n1 + 2) <= n2);
                boton = Funciones.selectBoton();
                jLabelOperacion.setText(n1 + " - " + n2);
                switch (boton) {
                    case 1:
                        botonCorrecto = 1;
                        jButtonSol1.setText(Integer.toString(n1 - n2));
                        jButtonSol2.setText(Integer.toString(n1 - n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 - n2 - Funciones.aleatorio()));
                        break;
                    case 2:
                        botonCorrecto = 2;
                        jButtonSol2.setText(Integer.toString(n1 - n2));
                        jButtonSol1.setText(Integer.toString(n1 - n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 - n2 - Funciones.aleatorio()));
                        break;
                    case 3:
                        botonCorrecto = 3;
                        jButtonSol3.setText(Integer.toString(n1 - n2));
                        jButtonSol1.setText(Integer.toString(n1 - n2 + Funciones.aleatorio()));
                        jButtonSol2.setText(Integer.toString(n1 - n2 - Funciones.aleatorio()));
                        break;
                }
                break;

            case 3:
                n1 = Funciones.obtenerNumeroP();
                n2 = Funciones.obtenerNumeroP();
                boton = Funciones.selectBoton();
                jLabelOperacion.setText(n1 + " * " + n2);
                switch (boton) {
                    case 1:
                        botonCorrecto = 1;
                        jButtonSol1.setText(Integer.toString(n1 * n2));
                        jButtonSol2.setText(Integer.toString(n1 * n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 * n2 - Funciones.aleatorio()));
                        break;
                    case 2:
                        botonCorrecto = 2;
                        jButtonSol2.setText(Integer.toString(n1 * n2));
                        jButtonSol1.setText(Integer.toString(n1 * n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 * n2 - Funciones.aleatorio()));
                        break;
                    case 3:
                        botonCorrecto = 3;
                        jButtonSol3.setText(Integer.toString(n1 * n2));
                        jButtonSol1.setText(Integer.toString(n1 * n2 + Funciones.aleatorio()));
                        jButtonSol2.setText(Integer.toString(n1 * n2 - Funciones.aleatorio()));
                        break;
                }
                break;

            case 4:
                do {
                    n1 = Funciones.obtenerNumero();
                    do {
                        n2 = Funciones.obtenerNumeroP();
                    } while (n2 == 0);
                } while (n1 % n2 != 0);
                boton = Funciones.selectBoton();
                jLabelOperacion.setText(n1 + " / " + n2);
                switch (boton) {
                    case 1:
                        botonCorrecto = 1;
                        jButtonSol1.setText(Integer.toString(n1 / n2));
                        jButtonSol2.setText(Integer.toString(n1 / n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 / n2 - Funciones.aleatorio()));
                        break;
                    case 2:
                        botonCorrecto = 2;
                        jButtonSol2.setText(Integer.toString(n1 / n2));
                        jButtonSol1.setText(Integer.toString(n1 / n2 + Funciones.aleatorio()));
                        jButtonSol3.setText(Integer.toString(n1 / n2 - Funciones.aleatorio()));
                        break;
                    case 3:
                        botonCorrecto = 3;
                        jButtonSol3.setText(Integer.toString(n1 / n2));
                        jButtonSol1.setText(Integer.toString(n1 / n2 + Funciones.aleatorio()));
                        jButtonSol2.setText(Integer.toString(n1 / n2 - Funciones.aleatorio()));
                        break;
                }
                break;

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelMensaje = new javax.swing.JPanel();
        jLabelmensaje = new javax.swing.JLabel();
        jButtonRestart = new javax.swing.JButton();
        jPanelReglas = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButtonStart = new javax.swing.JButton();
        jPanelJuego = new javax.swing.JPanel();
        jPanelCoches = new javax.swing.JPanel();
        jLabelC1 = new javax.swing.JLabel();
        jLabelC2 = new javax.swing.JLabel();
        jLabelC3 = new javax.swing.JLabel();
        jLabelC4 = new javax.swing.JLabel();
        jLabelOperacion = new javax.swing.JLabel();
        jButtonSol3 = new javax.swing.JButton();
        jButtonSol2 = new javax.swing.JButton();
        jButtonSol1 = new javax.swing.JButton();
        jLabelNube = new javax.swing.JLabel();
        jLabelFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanelMensaje.setOpaque(false);
        jPanelMensaje.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelmensaje.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabelmensaje.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelmensaje.setText("jLabel3");
        jPanelMensaje.add(jLabelmensaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 870, 80));

        jButtonRestart.setBackground(new java.awt.Color(204, 204, 255));
        jButtonRestart.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jButtonRestart.setText("VOLVER  A JUGAR");
        jButtonRestart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRestartActionPerformed(evt);
            }
        });
        jPanelMensaje.add(jButtonRestart, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 110, -1, 100));

        getContentPane().add(jPanelMensaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 870, 270));

        jPanelReglas.setOpaque(false);
        jPanelReglas.setPreferredSize(new java.awt.Dimension(680, 170));
        jPanelReglas.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Carrera de coches");
        jPanelReglas.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 6, 870, 37));

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Resuelve correctramente las operaciones para hacer que tu coche avance");
        jPanelReglas.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 49, 870, 30));

        jButtonStart.setBackground(new java.awt.Color(204, 204, 255));
        jButtonStart.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jButtonStart.setText("JUGAR");
        jButtonStart.setBorderPainted(false);
        jButtonStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStartActionPerformed(evt);
            }
        });
        jPanelReglas.add(jButtonStart, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 100, 130, 90));

        getContentPane().add(jPanelReglas, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 870, 220));

        jPanelJuego.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanelCoches.setOpaque(false);
        jPanelCoches.setLayout(null);
        jPanelCoches.add(jLabelC1);
        jLabelC1.setBounds(20, 0, 80, 80);
        jPanelCoches.add(jLabelC2);
        jLabelC2.setBounds(30, 40, 70, 70);
        jPanelCoches.add(jLabelC3);
        jLabelC3.setBounds(30, 90, 70, 70);
        jPanelCoches.add(jLabelC4);
        jLabelC4.setBounds(10, 150, 90, 40);

        jPanelJuego.add(jPanelCoches, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 420, 870, 240));

        jLabelOperacion.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabelOperacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanelJuego.add(jLabelOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 60, 260, 50));

        jButtonSol3.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jButtonSol3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSol3ActionPerformed(evt);
            }
        });
        jPanelJuego.add(jButtonSol3, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 130, 80, 50));

        jButtonSol2.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jButtonSol2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSol2ActionPerformed(evt);
            }
        });
        jPanelJuego.add(jButtonSol2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 130, 80, 50));

        jButtonSol1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jButtonSol1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSol1ActionPerformed(evt);
            }
        });
        jPanelJuego.add(jButtonSol1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 80, 50));
        jPanelJuego.add(jLabelNube, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 420, 230));
        jPanelJuego.add(jLabelFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 860, 650));

        getContentPane().add(jPanelJuego, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Evento del boton que inicia el juego
     * @param evt 
     */
    private void jButtonStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStartActionPerformed
        jPanelReglas.setVisible(false);
        jPanelCoches.setVisible(true);
        jLabelNube.setVisible(true);
        jButtonSol1.setVisible(true);
        jButtonSol2.setVisible(true);
        jButtonSol3.setVisible(true);
        jLabelOperacion.setVisible(true);
        hilo.start();
        operacion();
    }//GEN-LAST:event_jButtonStartActionPerformed

 
    private void jButtonSol2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSol2ActionPerformed
        switch (botonCorrecto) {
            case 2:
                operacion();
                jLabelC1.setLocation((jLabelC1.getX() + Funciones.avanzar()), jLabelC1.getY());
                break;

        }
    }//GEN-LAST:event_jButtonSol2ActionPerformed

    private void jButtonSol1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSol1ActionPerformed
        switch (botonCorrecto) {
            case 1:
                operacion();
                jLabelC1.setLocation((jLabelC1.getX() + Funciones.avanzar()), jLabelC1.getY());
                break;
        }
    }//GEN-LAST:event_jButtonSol1ActionPerformed

    private void jButtonSol3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSol3ActionPerformed
        switch (botonCorrecto) {
            case 3:
                operacion();
                jLabelC1.setLocation((jLabelC1.getX() + Funciones.avanzar()), jLabelC1.getY());
                break;
        }
    }//GEN-LAST:event_jButtonSol3ActionPerformed

    /**
     * Evento del boton que reinicia el juego.
     * @param evt 
     */
    private void jButtonRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRestartActionPerformed

        Juego.main(null);
        dispose();
    }//GEN-LAST:event_jButtonRestartActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Juego dialog = new Juego(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRestart;
    private javax.swing.JButton jButtonSol1;
    private javax.swing.JButton jButtonSol2;
    private javax.swing.JButton jButtonSol3;
    private javax.swing.JButton jButtonStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelC1;
    private javax.swing.JLabel jLabelC2;
    private javax.swing.JLabel jLabelC3;
    private javax.swing.JLabel jLabelC4;
    private javax.swing.JLabel jLabelFondo;
    private javax.swing.JLabel jLabelNube;
    private javax.swing.JLabel jLabelOperacion;
    private javax.swing.JLabel jLabelmensaje;
    private javax.swing.JPanel jPanelCoches;
    private javax.swing.JPanel jPanelJuego;
    private javax.swing.JPanel jPanelMensaje;
    private javax.swing.JPanel jPanelReglas;
    // End of variables declaration//GEN-END:variables

    /**
     * Metodo que mueve los coches oponentes cada X tiempo.
     */
    @Override
    public void run() {

        while ((coches[0].getX() < 750) && (coches[1].getX() < 750) && (coches[2].getX() < 750)) {

            if ((jLabelC1.getX() > 750)) {
                jLabelNube.setVisible(false);
                jButtonSol1.setVisible(false);
                jButtonSol2.setVisible(false);
                jButtonSol3.setVisible(false);
                jLabelOperacion.setVisible(false);
                jLabelmensaje.setText("Has ganado!");
                jPanelMensaje.setVisible(true);

                hilo.stop();

            }
            try {
                hilo.sleep(MenuCarrera.dicifultad);
            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
            for (int i = 0; i < coches.length; i++) {
                coches[i].setLocation((coches[i].getX() + Funciones.avanzar()), coches[i].getY());

            }

        }

        jLabelNube.setVisible(false);
        jButtonSol1.setVisible(false);
        jButtonSol2.setVisible(false);
        jButtonSol3.setVisible(false);
        jLabelOperacion.setVisible(false);
        jPanelMensaje.setVisible(true);
        jLabelmensaje.setText("Has perdido!");

    }

}
