/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retoDiscalculia.carreraCoches;

/**
 *
 * @author José Ángel Lavín Fernández
 */
public class Funciones {

    /**
     * Metodo que devuelve un numero entero perteneciente a uno de los valores
     * del array, el cual se utilizara para mover un coche a lo largo del eje X.
     *
     * @return n - numero entero
     */
    public static int avanzar() {
        int[] numero = {20, 40, 60};
        int n = (int) (Math.random() * 3);
        return numero[n];
    }

    /**
     * Metodo que devuelve un numero entero comprendido entre el 0 y el 19, el
     * cual se utilizara para realizar las operaciones matematicas.
     *
     * @return n - numero entero
     */
    public static int obtenerNumero() {
        int n = (int) (Math.random() * 20);
        return n;
    }

    /**
     * Metodo que devuelve un numero entero comprendido entre el 0 y el 9, el
     * cual se utilizara para realizar las operaciones matematicas.
     *
     * @return n - numero entero
     */
    public static int obtenerNumeroP() {
        int n = (int) (Math.random() * 10);
        return n;
    }

    /**
     * Metodo que devuelve un numero entero comprendido entre el 1 y el 5, el
     * cual se utilizara para crear las opciones incorrectas de la operacion
     * matematica.
     *
     * @return n - numero entero
     */
    public static int aleatorio() {
        int n = (int) (Math.random() * 5) + 1;
        return n;
    }

    /**
     * Metodo que devuelve un numero entero perteneciente a uno de los valores
     * del array, el cual se utilizara para determinar en que boton se
     * posiciona lo solucion correcta de la operacion matematica.
     *
     * @return n - numero entero
     */
    public static int selectBoton() {
        int[] numero = {1, 2, 3};
        int n = (int) (Math.random() * 3);
        return numero[n];
    }
}
