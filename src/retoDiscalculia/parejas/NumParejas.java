/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retoDiscalculia.parejas;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author futbo
 */
public class NumParejas {
    
    /**
     * Compone la pareja de numero1 de 2 cartas cuyos valores sumen 10
     */
    
    protected int[] pareja1;
    /**
     * Compone la pareja de numero2 de 2 cartas cuyos valores sumen 10
     */
    protected int[] pareja2;
    /**
     * Compone la pareja de numero3 de 2 cartas cuyos valores sumen 10
     */
    protected int[] pareja3;
    /**
     * Compone la pareja de numero4 de 2 cartas cuyos valores sumen 10
     */
    protected int[] pareja4;
    /**
     * Compone la pareja de numero5 de 2 cartas cuyos valores sumen 10
     */
    protected int[] pareja5;
    /**
     * Compone la pareja de numero6 de 2 cartas cuyos valores sumen 10
     */
    protected int[] pareja6;
    /**
     * Compone la pareja de numero7 de 2 cartas cuyos valores sumen 10
     */
    protected int[] pareja7;
    /**
     * Compone la pareja de numero8 de 2 cartas cuyos valores sumen 10
     */
    protected int[] pareja8;
    
    /**
     * Establece los valores de las parejas aleatoriamente, generando el primer numero aleatorio y despues restando el primer numero
     * a 10 para generar el segundo numero
     */
    
    public NumParejas() {
        pareja1 = new int[2];
        pareja2 = new int[2];
        pareja3 = new int[2];
        pareja4 = new int[2];
        pareja5 = new int[2];
        pareja6 = new int[2];
        pareja7 = new int[2];
        pareja8 = new int[2];
        
        pareja1[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja1[1] =  10-pareja1[0];
        
        pareja2[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja2[1] =  10-pareja2[0];
        
        pareja3[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja3[1] =  10-pareja3[0];
        
        pareja4[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja4[1] =  10-pareja4[0];
        
        pareja5[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja5[1] =  10-pareja5[0];
        
        pareja6[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja6[1] =  10-pareja6[0];
        
        pareja7[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja7[1] =  10-pareja7[0];
        
        pareja8[0] = (int) Math.floor(Math.random() * (9 - 1)) + 1;
        pareja8[1] =  10-pareja8[0];
        
    }

    /**
     * 
     * @return devuelve los valores de la pareja1
     */
    public int[] getPareja1() {
        return pareja1;
    }
    /**
     * 
     * @return devuelve los valores de la pareja2
     */
    public int[] getPareja2() {
        return pareja2;
    }
    /**
     * 
     * @return devuelve los valores de la pareja3
     */
    public int[] getPareja3() {
        return pareja3;
    }
    /**
     * 
     * @return devuelve los valores de la pareja4
     */
    public int[] getPareja4() {
        return pareja4;
    }
    /**
     * 
     * @return devuelve los valores de la pareja5
     */
    public int[] getPareja5() {
        return pareja5;
    }
    /**
     * 
     * @return devuelve los valores de la pareja6
     */
    public int[] getPareja6() {
        return pareja6;
    }
    /**
     * 
     * @return devuelve los valores de la pareja7
     */
    public int[] getPareja7() {
        return pareja7;
    }
    /**
     * 
     * @return devuelve los valores de la pareja8
     */
    public int[] getPareja8() {
        return pareja8;
    }

    
}
